# Building

For ease of development, this project has transitioned from CSS to SASS, and includes files for building with Gulp.

## Dependencies
You'll need an environment running Node. Optionally, you may use Yarn to install the packages needed to get it running. I haven't tested it with Node's default package manager NPM.

## The short instructions

```
cd lpzone-site-customisations/
mkdir build
yarn install
gulp styles
```

If you want to make changes and have the resulting CSS file automatically update every time you save, just run `gulp` in substitution of `gulp styles` and make as many changes as you like.