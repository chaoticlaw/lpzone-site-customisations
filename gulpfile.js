'use strict';

var gulp = require('gulp')
  , sass = require('gulp-sass')
  , autoprefixer = require('gulp-autoprefixer')
  , notify = require('gulp-notify')
  , del = require('del');

gulp.task('styles', function() {
  return gulp.src('src/[^_]*.scss')
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(autoprefixer({ browsers: ['last 2 versions', 'firefox>=4', 'safari 7', 'safari 8', 'IE 8', 'IE 9', 'IE 10', 'IE 11']}, {cascade: false}))
    .pipe(gulp.dest('build/'))
    .pipe(notify({ message: 'SCSS converted to CSS, prefixed and copied to /build/'}))
});

gulp.task('clean', function(){
  return del(['build/*.css']);
});

gulp.task('watch', function() {
  gulp.watch('src/*.scss', ['styles']);
});

gulp.task('default', ['clean'], function() {
  gulp.start('styles', 'watch');
});